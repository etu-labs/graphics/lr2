import {addLight, addSphere, mainScene} from "./drawer.js";
import {Tools} from "./tools.js";

const buttonShow = document.getElementById('show-menu-button')
const addEntityLabel = document.getElementById('add-entity-label')
const entitySelect = document.getElementById('entity-select')

class Menu {
    static #isShow = true

    static init() {
        Tools.initSettings()

        buttonShow.addEventListener('click', () => {
            Menu.toggle()
        })

        mainScene.entities.forEach(e => Tools.connect(e, e.name))

        addEntityLabel.onclick = (e) => {
            const selected = entitySelect.value
            if (selected === 'Sphere') addSphere()
            else addLight()
            Tools.connect(mainScene.entities.at(-1), mainScene.entities.at(-1).name)
        }
    }

    static show() {
        this.#isShow = true
        const menu = document.getElementById('menu')
        menu.classList.remove('hide-menu')
        buttonShow.innerHTML = '&rarr;'
    }

    static hide() {
        this.#isShow = false
        const menu = document.getElementById('menu')
        menu.classList.add('hide-menu')
        buttonShow.innerHTML = '&larr;'
    }

    static toggle() {
        if (this.#isShow) this.hide()
        else this.show()
    }
}

export default Menu
