import {Keyboard} from "./keyboard.js";
import {deltaTime} from "./drawer.js";

export let CameraMovement = {
    FORWARD: 0,
    BACKWARD: 1,
    LEFT: 2,
    RIGHT: 3
}

// Default
const YAW = -75.0;
const PITCH = 0.0;
const SPEED = 0.005;
const SENSITIVITY = 0.5;
const ZOOM = 45.0;

export class Camera {
    Position = {};
    Front = glm.vec3(0.0, 0.0, -1.0);
    Up = {};
    Right = {};
    WorldUp = {};

    Yaw = YAW;
    Pitch = PITCH;
    MovementSpeed = SPEED;
    MouseSensitivity = SENSITIVITY;
    Zoom = ZOOM;

    constructor(position, up, yaw, pitch) {
        this.Position = position || glm.vec3(0.0, 0.0, 0.0);
        this.WorldUp = up || glm.vec3(0.0, 1.0, 0.0);
        if (yaw) this.Yaw = yaw;
        if (pitch) this.Pitch = pitch;
        const keyboard = Keyboard.getInstance()
        keyboard.addKeyListener('KeyW', () => {     // W
            this.ProcessKeyboard(CameraMovement.FORWARD, deltaTime * 1.0);
        });
        keyboard.addKeyListener('KeyA', () => {     // A
            this.ProcessKeyboard(CameraMovement.LEFT, deltaTime * 1.0);
        });
        keyboard.addKeyListener('KeyS', () => {     // S
            this.ProcessKeyboard(CameraMovement.BACKWARD, deltaTime * 1.0);
        });
        keyboard.addKeyListener('KeyD', () => {     // D
            this.ProcessKeyboard(CameraMovement.RIGHT, deltaTime * 1.0);
        });

        this.#updateCameraVectors();
    }

    GetViewMatrix() {
        return glm.lookAt(this.Position, this.Position['+'](this.Front), this.Up);
    }

    ProcessKeyboard(direction, deltaTime) {
        let velocity = this.MovementSpeed * deltaTime;
        if (direction == CameraMovement.FORWARD)
            this.Position['+='](this.Front['*'](velocity));
        if (direction == CameraMovement.BACKWARD)
            this.Position['-='](this.Front['*'](velocity));
        if (direction == CameraMovement.LEFT)
            this.Position['-='](this.Right['*'](velocity));
        if (direction == CameraMovement.RIGHT)
            this.Position['+='](this.Right['*'](velocity));
    }

    ProcessMouseMovement(xoffset, yoffset) {
        xoffset *= this.MouseSensitivity;
        yoffset *= this.MouseSensitivity;

        this.Yaw += xoffset;
        this.Pitch += yoffset;

        this.#updateCameraVectors();
    }

    #updateCameraVectors() {
        let front = glm.vec3();
        front.x = Math.cos(glm.radians(this.Yaw)) * Math.cos(glm.radians(this.Pitch));
        front.y = Math.sin(glm.radians(this.Pitch));
        front.z = Math.sin(glm.radians(this.Yaw)) * Math.cos(glm.radians(this.Pitch));
        this.Front = glm.normalize(front);
        this.Right = glm.normalize(glm.cross(this.Front, this.WorldUp));
        this.Up = glm.normalize(glm.cross(this.Right, this.Front));
    }
}
