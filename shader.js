import { Read as fileRead } from './fileLoader.js';
import {gl} from "./drawer.js";

export class Shader {
    async initShader(shadersNames) {
        if (this.#program) return;

        console.log(gl.getParameter(gl.SHADING_LANGUAGE_VERSION));

        let shaders = [];

        for (let shaderName of shadersNames) {
            let filePath = './shaders/' + shaderName;

            let source = await fileRead(filePath);

            let shader = gl.createShader(shaderName.split('.').pop() === 'vert' ?
                gl.VERTEX_SHADER : gl.FRAGMENT_SHADER);
            gl.shaderSource(shader, source);
            gl.compileShader(shader);
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                let infoLog = gl.getShaderInfoLog(shader);
                throw new Error(`ERROR::SHADER::${shaderName.split('.').pop()}::COMPILATION_FAILED\n${infoLog}\n`);
            }

            shaders.push(shader);
        }

        this.#program = gl.createProgram();
        for (let shader of shaders) {
            gl.attachShader(this.#program, shader);
        }
        gl.linkProgram(this.#program);
        if (!gl.getProgramParameter(this.#program, gl.LINK_STATUS)) {
            let infoLog = gl.getProgramInfoLog(this.#program);
            throw new Error(`ERROR::SHADER::PROGRAM::LINKING_FAILED\n${infoLog}\n`);
        }

        for (let shader of shaders) {
            gl.deleteShader(shader);
        }

        return this;
    }

    use() {
        gl.useProgram(this.#program);
    }

    getAttribLocation(name) {
        return gl.getAttribLocation(this.#program, name);
    }

    setFloat(name, value, useShader) {
        if (useShader) this.use();
        gl.uniform1f(this.#getUniformLocation(name), value);
    }

    setInteger(name, value, useShader) {
        if (useShader) this.use();
        gl.uniform1i(this.#getUniformLocation(name), value);
    }

    setVector2f(name, value, useShader) {
        if (useShader) this.use();
        gl.uniform2f(this.#getUniformLocation(name), value.x, value.y);
    }

    setVector3f(name, value, useShader) {
        if (useShader) this.use();
        gl.uniform3f(this.#getUniformLocation(name), value.x, value.y, value.z);
    }

    setVector4f(name, value, useShader) {
        if (useShader) this.use();
        gl.uniform4f(this.#getUniformLocation(name), value.x, value.y, value.z, value.w);
    }

    setMatrix4(name, value, useShader) {
        if (useShader) this.use();
        gl.uniformMatrix4fv(this.#getUniformLocation(name), false, value);
    }

    // private
    #program = null;
    #namesUniformsLocations = new Map();

    #getUniformLocation(name) {
        if (this.#namesUniformsLocations.has(name))
            return this.#namesUniformsLocations.get(name);

        let location = gl.getUniformLocation(this.#program, name);
        this.#namesUniformsLocations.set(name, location);
        return location;
    }
}
