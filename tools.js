import {settings} from './drawer.js'
import {Texture} from "./entity/properties/texture.js";
import {Light} from "./entity/properties/light.js";

export class Tools {
    static #toolsRootElement = document.getElementById('tools')

    static connect(object, toolsName) {
        const rootObject = document.createElement('div')

        const title = document.createElement('h4')
        title.innerText = toolsName
        rootObject.appendChild(title)
        const scaleRange = this.#createRange(
            'Размер',
            (val) => object.scale = (glm.vec3(val)),
            {
                max: 2.0,
                min: 0.1,
                step: 0.05,
                value: 1.0
            }
        )
        const xRange = this.#createRange(
            'X',
            (val) => object.position = (glm.vec3(val, object.position.y, object.position.z)),
            {
                max: 2.0,
                min: -2.0,
                step: 0.1,
                value: object.position.x
            }
        )
        const yRange = this.#createRange(
            'Y',
            (val) => object.position = (glm.vec3(object.position.x, val, object.position.z)),
            {
                max: 2.0,
                min: -2.0,
                step: 0.1,
                value: object.position.y
            }
        )
        const zRange = this.#createRange(
            'Z',
            (val) => object.position = (glm.vec3(object.position.x, object.position.y, val)),
            {
                max: 2.0,
                min: -2.0,
                step: 0.1,
                value: object.position.z
            }
        )

        const selectTexture = this.createTextureSelect((val) => object.addProperty(new Texture({name: val})))
        const selectUpload = this.createTextureUpload((val) => object.addProperty(new Texture({data: val})))
        const lightToggle = this.createLightToggle(object.properties.has(Light.name), (val) => {
                if (val) object.addProperty(new Light())
                else object.removeProperty(Light.name)
            }
        )

        rootObject.appendChild(scaleRange)
        rootObject.appendChild(xRange)
        rootObject.appendChild(yRange)
        rootObject.appendChild(zRange)
        rootObject.appendChild(lightToggle)
        rootObject.appendChild(selectTexture)
        rootObject.appendChild(selectUpload)
        rootObject.className = 'object-tools'
        this.#toolsRootElement.appendChild(rootObject)
    }

    static #createRange(title, listener, options) {
        const wrapper = document.createElement('div')
        wrapper.className = 'rangeWrapper'
        const label = document.createElement('label')
        const name = document.createElement('b')
        name.innerText = `${title}`
        const range = document.createElement('i')
        range.innerText = ` (${options.min}; ${options.max})`
        label.appendChild(name)
        label.appendChild(range)
        const input = document.createElement('input')
        input.type = 'range'
        input.max = options.max
        input.min = options.min
        input.step = options.step
        input.value = options.value
        input.addEventListener('input', (e) => listener(parseFloat(e.target.value)))
        wrapper.appendChild(label)
        wrapper.appendChild(input)
        return wrapper
    }

    static createTextureSelect(listener) {
        const wrapper = document.createElement('div')
        const select = document.createElement('select')
        const options = new Array(5).fill(0).map((_, i) => {
            const option = document.createElement('option')
            option.text = `wood${i + 1}.jpeg`
            option.value = `wood${i + 1}.jpeg`
            return option
        })
        options.forEach(o => select.appendChild(o))
        select.onchange = (e) => listener(e.target.value)
        wrapper.appendChild(select)
        return wrapper
    }

    static createTextureUpload(listener) {
        const wrapper = document.createElement('div')
        const upload = document.createElement('input')
        upload.type = 'file'
        upload.onchange = (e) => {
            const file = e.target.files[0]
            console.log(file)
            listener(file)
        }
        wrapper.appendChild(upload)
        return wrapper
    }

    static createLightToggle(checked, listener) {
        const wrapper = document.createElement('div')
        const checkbox = document.createElement('input')
        const title = document.createElement('span')
        title.textContent = 'Light'
        checkbox.type = 'checkbox'
        checkbox.checked = checked
        checkbox.onchange = (e) => listener(e.target.checked)
        wrapper.appendChild(checkbox)
        wrapper.appendChild(title)
        return wrapper
    }

    static initSettings() {
        const form = document.projectionForm
        const projection = [...form.projection].find(x => x.value === settings.projection)
        const light = [...document.lightForm.light].find(x => x.value === settings.light)
        projection.checked = true
        light.checked = true
        form.addEventListener('change', (e) => settings.projection = e.target.value)
        document.lightForm.addEventListener('change', (e) => settings.light = e.target.value)
    }
}
