export { Read };
let baseDirectory = '/data';

async function Read(fileName) {
    return await fetch(concatUrl([fileName])).then(res => res.text());
}

function concatUrl(path) {
    return [baseDirectory, ...path].join('/');
}

