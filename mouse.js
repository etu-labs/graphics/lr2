export function initMouse(camera) {
    const canvas = document.querySelector('canvas');
    let isPressed = false;
    let x = 0;
    let y = 0;

    canvas.addEventListener('mousemove', (e) => {
        if (isPressed) {
            camera.ProcessMouseMovement(e.clientX - x, (y - e.clientY));
            x = e.clientX
            y = e.clientY
        }
    });

    canvas.addEventListener('mousedown', (e) => {
        isPressed = true;
        x = e.clientX
        y = e.clientY
    });

    canvas.addEventListener('mouseup', (e) => {
        isPressed = false;
    });
}

