import {Shader} from "./shader.js"
import {Camera} from "./camera.js"
import {initMouse} from "./mouse.js"
import {ObjParser} from "./objParser.js"
import {Scene} from "./scene.js"
import {Entity} from "./entity/entity.js"
import {Model} from "./entity/properties/model.js";
import {Material} from "./entity/properties/material.js";
import {Keyboard} from "./keyboard.js";
import {Light} from "./entity/properties/light.js";
import {Texture} from "./entity/properties/texture.js";

export let gl = {}

export const canvasSize = {
    width: 1920,
    height: 1080
};
let canvas = {}

let objectShader = new Shader()
let objectPreviewShader = new Shader()
export const mainScene = new Scene()

let sphere = null;


export let deltaTime = 0

export const settings = {
    projection: 'perspective',
    light: 'fong'
}

export function CanvasPreparation () {
    canvas = document.getElementById('main-canvas')
    if (!canvas) throw new Error("Canvas not found")
    canvas.width = document.body.clientWidth
    canvas.height = document.body.clientHeight
    canvasSize.width = document.body.clientWidth
    canvasSize.height = document.body.clientHeight
    canvas.style.width = '100vw'
    canvas.style.height = '100vh'
}

export function GLInit() {
    gl = canvas.getContext('webgl2')
    if (!gl) throw new Error("Webgl2 not found")
}

export async function InitComponents() {
    gl.viewport(0, 0, canvasSize.width, canvasSize.height)
    gl.enable(gl.DEPTH_TEST)

    await objectShader.initShader(['object.vert', 'object.frag'])
    await objectPreviewShader.initShader(['objectPreview.vert', 'objectPreview.frag'])
    mainScene.shader = objectShader
    const data = await ObjParser.load('globe-sphere.obj')
    sphere = ObjParser.parse(data.position, data.normal, data.texcoord)
}

export async function InitScene() {
    let camera = new Camera(glm.vec3(-1.0, 0.0, 3.5))
    mainScene.addCamera(camera)
    initMouse(camera)

    addSphere()
    addLight('Light 1')
    addLight('Light 2', glm.vec3(-1.0, 1.0, -1.0))
}

export function addLight(name, position) {
    const light = new Entity(position || glm.vec3(1.2, 0.0, 2.0), null, glm.vec3(0.2))
    light.name = name || `Light ${mainScene.entities.length}`
    light.addProperty(new Model())
    light.addProperty(new Material( glm.vec3(1.0), glm.vec3(1.0), glm.vec3(1.0), 32.0))
    light.addProperty(new Light())
    mainScene.addEntity(light)
}

export function addSphere() {
    if (!sphere) return
    const entity = new Entity()
    entity.name = "Sphere " + mainScene.entities.length
    entity.addProperty(new Model(sphere))
    entity.addProperty(new Material())
    entity.addProperty(new Texture({name: 'wood4.jpeg'}))
    mainScene.addEntity(entity)
}

export function Loop() {
    let lastFrame = 0

    let loop = (currentFrame) => {
        deltaTime = currentFrame - lastFrame
        lastFrame = currentFrame

        Keyboard.getInstance().update()
        if (settings.light === 'guro') {
            mainScene.shader = objectPreviewShader
        } else {
            mainScene.shader = objectShader
        }
        mainScene.draw()
        window.requestAnimationFrame(loop);
    }

    loop(0);
}
