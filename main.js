import {CanvasPreparation, GLInit, InitScene, InitComponents, Loop} from './drawer.js';
import Menu from './menu.js'

export async function init() {
    try {
        CanvasPreparation();
        GLInit();
        await InitComponents();
        await InitScene();
        Menu.init()
        Loop();
    } catch (e) {
        console.log(e);
    }
}

// document.getElementById('button-create').addEventListener('click', createCube)

init();
