export class Keyboard {
    #listeners = new Map()
    #pressed = []
    static #instance = null

    constructor() {
        const body = document.querySelector('body')

        body.addEventListener('keydown', (e) => {
            let index = this.#pressed.findIndex(element => {
                return element === e.code
            })
            if (index === -1) {
                this.#pressed.push(e.code)
            }
        })

        body.addEventListener('keyup', (e) => {
            let index = this.#pressed.findIndex(element => {
                return element === e.code
            })
            if (index !== -1) {
                this.#pressed.splice(index, 1)
            }
        })
    }

    static getInstance() {
        if (!this.#instance) {
            this.#instance = new Keyboard()
        }

        return this.#instance
    }

    addKeyListener(key, callback) {
        this.#listeners.set(key, callback)
    }

    update() {
        this.#pressed.forEach(e => {
            if (this.#listeners.has(e)) {
                this.#listeners.get(e)()
            }
        })
    }
}
