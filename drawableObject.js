import {gl} from "./drawer.js";
import {Tools} from "./tools.js";

const VERTICES = [
    -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,
    0.5, -0.5, -0.5,  0.0,  0.0, -1.0,
    0.5,  0.5, -0.5,  0.0,  0.0, -1.0,
    0.5,  0.5, -0.5,  0.0,  0.0, -1.0,
    -0.5,  0.5, -0.5,  0.0,  0.0, -1.0,
    -0.5, -0.5, -0.5,  0.0,  0.0, -1.0,

    -0.5, -0.5,  0.5,  0.0,  0.0,  1.0,
    0.5, -0.5,  0.5,  0.0,  0.0,  1.0,
    0.5,  0.5,  0.5,  0.0,  0.0,  1.0,
    0.5,  0.5,  0.5,  0.0,  0.0,  1.0,
    -0.5,  0.5,  0.5,  0.0,  0.0,  1.0,
    -0.5, -0.5,  0.5,  0.0,  0.0,  1.0,

    -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,
    -0.5,  0.5, -0.5, -1.0,  0.0,  0.0,
    -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,
    -0.5, -0.5, -0.5, -1.0,  0.0,  0.0,
    -0.5, -0.5,  0.5, -1.0,  0.0,  0.0,
    -0.5,  0.5,  0.5, -1.0,  0.0,  0.0,

    0.5,  0.5,  0.5,  1.0,  0.0,  0.0,
    0.5,  0.5, -0.5,  1.0,  0.0,  0.0,
    0.5, -0.5, -0.5,  1.0,  0.0,  0.0,
    0.5, -0.5, -0.5,  1.0,  0.0,  0.0,
    0.5, -0.5,  0.5,  1.0,  0.0,  0.0,
    0.5,  0.5,  0.5,  1.0,  0.0,  0.0,

    -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,
    0.5, -0.5, -0.5,  0.0, -1.0,  0.0,
    0.5, -0.5,  0.5,  0.0, -1.0,  0.0,
    0.5, -0.5,  0.5,  0.0, -1.0,  0.0,
    -0.5, -0.5,  0.5,  0.0, -1.0,  0.0,
    -0.5, -0.5, -0.5,  0.0, -1.0,  0.0,

    -0.5,  0.5, -0.5,  0.0,  1.0,  0.0,
    0.5,  0.5, -0.5,  0.0,  1.0,  0.0,
    0.5,  0.5,  0.5,  0.0,  1.0,  0.0,
    0.5,  0.5,  0.5,  0.0,  1.0,  0.0,
    -0.5,  0.5,  0.5,  0.0,  1.0,  0.0,
    -0.5,  0.5, -0.5,  0.0,  1.0,  0.0
];

const POSITION = glm.vec3(0.0, 0.0, 0.0);
const ROTATE = {
    angle: 0,
    vector: glm.vec3(0.0, 0.0, 0.0)
}
const SCALE = glm.vec3(1.0, 1.0, 1.0);

export class DrawableObject {
    #vertices = null;
    #numberVertices = 0;
    Position = glm.vec3();
    #rotate = {
        angle: 0,
        vector: glm.vec3()
    }
    #scale = glm.vec3();
    Material = {
        ambient: {
            x: 1.0,
            y: 0.5,
            z: 0.3
        },
        diffuse: {
            x: 1.0,
            y: 0.5,
            z: 0.3
        },
        specular: {
            x: 0.5,
            y: 0.5,
            z: 0.5
        },
        shininess: 32.0
    }

    #shader = null;
    #VAO = null;
    #VBO = null;


    constructor(shader, vertices, position, rotate, scale) {
        if (!shader) throw new Error('Сan\'t create an object without a shader')
        this.#shader = shader;
        this.#vertices = vertices || VERTICES;
        this.#numberVertices = this.#vertices.length / 6;
        this.Position = position ?? POSITION;
        this.#rotate = rotate ?? ROTATE;
        this.#scale = scale ?? SCALE;

        this.#VAO = gl.createVertexArray();
        this.#VBO = gl.createBuffer();
        let verticesFloat32Array = new Float32Array(this.#vertices);
        let FSIZE = verticesFloat32Array.BYTES_PER_ELEMENT;

        gl.bindBuffer(gl.ARRAY_BUFFER, this.#VBO);
        gl.bufferData(gl.ARRAY_BUFFER, verticesFloat32Array, gl.STATIC_DRAW);

        gl.bindVertexArray(this.#VAO);
        let attrPosLoc = this.#shader.GetAttribLocation("aPos");
        gl.vertexAttribPointer(attrPosLoc, 3, gl.FLOAT, false, FSIZE * 6, 0);
        gl.enableVertexAttribArray(attrPosLoc);

        let attrNormalLoc = this.#shader.GetAttribLocation("aNormal");
        gl.vertexAttribPointer(attrNormalLoc, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 3);
        gl.enableVertexAttribArray(attrNormalLoc);

        gl.bindVertexArray(null);
    }

    Draw(projection, view) {
        if (!this.#vertices) return null;

        this.#shader.SetMatrix4("projection", projection.elements);
        this.#shader.SetMatrix4("view", view.elements);

        // material properties
        this.#shader.SetVector3f("material.ambient", this.Material.ambient);
        this.#shader.SetVector3f("material.diffuse", this.Material.diffuse);
        this.#shader.SetVector3f("material.specular", this.Material.specular);
        this.#shader.SetFloat("material.shininess", this.Material.shininess);

        let model = glm.mat4(1.0);
        model = glm.translate(model, this.Position);
        model = glm.scale(model, this.#scale);
        model = glm.rotate(model, this.#rotate.angle, this.#rotate.vector);
        this.#shader.SetMatrix4("model", model.elements);

        gl.bindVertexArray(this.#VAO);
        gl.drawArrays(gl.TRIANGLES, 0, this.#numberVertices);
        gl.bindVertexArray(null);
    }

    setScale(scale) {
        this.#scale = scale
    }

    setPosition(position) {
        this.Position = position
    }

    connectTools(toolsId) {
        Tools.connect(this, toolsId)
    }
}
