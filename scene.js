import {canvasSize, gl, settings} from "./drawer.js";

export class Scene {
    shader = null

    cameras = []
    currentCamera = null

    entities = []
    lights = []

    directionalLight = {
        direction: glm.vec3(-0.2, -1.0, -0.3),
        ambient: glm.vec3(0.05, 0.05, 0.05),
        diffuse: glm.vec3(0.4, 0.4, 0.4),
        specular: glm.vec3(0.5, 0.5, 0.5)
    }

    backgroundColor = {
        r: this.directionalLight.diffuse.x,
        g: this.directionalLight.diffuse.y,
        b: this.directionalLight.diffuse.z,
        a: 1.0
    }

    draw() {
        if (!this.shader) return

        gl.clearColor(this.backgroundColor.r, this.backgroundColor.g, this.backgroundColor.b, this.backgroundColor.a);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        this.shader.use()

        this.shader.setMatrix4("projection", this.#getProjection(settings.projection).elements)
        this.shader.setMatrix4("view", this.currentCamera.GetViewMatrix().elements)
        this.shader.setVector3f("viewPos", this.currentCamera.Position);

        this.shader.setVector3f("dirLight.direction", this.directionalLight.direction);
        this.shader.setVector3f("dirLight.ambient", this.directionalLight.ambient);
        this.shader.setVector3f("dirLight.diffuse", this.directionalLight.diffuse);
        this.shader.setVector3f("dirLight.specular", this.directionalLight.specular);

        this.shader.setInteger("numberPointLights", this.lights.length);
        this.lights.forEach((entity, index) => {
            let lightProperty = entity.properties.get("Light")

            this.shader.setVector3f(`pointLights[${index}].position`, entity.position)
            this.shader.setFloat(`pointLights[${index}].constant`, lightProperty.constant)
            this.shader.setFloat(`pointLights[${index}].linear`, lightProperty.linear)
            this.shader.setFloat(`pointLights[${index}].quadratic`, lightProperty.quadratic)
            this.shader.setVector3f(`pointLights[${index}].ambient`,
                lightProperty.material ? lightProperty.material.ambient : lightProperty.ambient)
            this.shader.setVector3f(`pointLights[${index}].diffuse`,
                lightProperty.material ? lightProperty.material.diffuse : lightProperty.diffuse)
            this.shader.setVector3f(`pointLights[${index}].specular`,
                lightProperty.material ? lightProperty.material.specular : lightProperty.specular)

        })

        this.entities.forEach(entity => {
            entity.draw()
        })
    }

    addCamera(camera) {
        this.cameras.push(camera)
        if (!this.currentCamera) {
            this.currentCamera = camera
        }
        return this.cameras.length - 1
    }

    removeCamera(index) {
        this.cameras.splice(index, 1)
    }

    setCurrentCamera(camera) {
        this.currentCamera = camera
    }

    addEntity(entity) {
        if (!this.shader) return

        this.entities.push(entity)
        let index = this.entities.length - 1
        if (!entity.name) {
            entity.name = "Entity" + index
        }
        entity.scene = this
        entity.init()

        return index
    }

    removeEntity(index) {
        this.entities[index].destroy()
        this.entities.splice(index, 1)
    }

    removeEntityByName(name) {
        this.entities.splice(this.entities.findIndex((entity) => entity.name !== name), 1)
    }


    #getProjection(type) {
        if (type === 'perspective') {
            return glm.perspective(glm.radians(this.currentCamera.Zoom), canvasSize.width / canvasSize.height, 0.1, 100.0);
        } else if (type === 'ortho') {
            return glm.ortho(-1.0, 1.0, -1.0, 1.0, 0.1, 100.0);
        }
    }
}
