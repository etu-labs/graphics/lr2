export class Entity {
    name = ""

    scene = null

    position = glm.vec3(0.0, 0.0, 0.0)
    rotate = {
        angle: 0,
        vector: glm.vec3(0.0, 0.0, 0.0)
    }
    scale = glm.vec3(1.0, 1.0, 1.0)

    properties = new Map()


    constructor(position, rotate, scale) {
        if (position) {
            this.position = position
        }
        if (rotate) {
            this.rotate = rotate
        }
        if (scale) {
            this.scale = scale
        }
    }

    init() {
        this.properties.forEach((property) => {
            property.init()
        })
    }

    destroy() {
        this.properties.forEach((property) => {
            property.destroy()
        })
    }

    draw() {
        if (!this.scene) return

        let model = glm.mat4(1.0)
        model = glm.translate(model, this.position)
        model = glm.scale(model, this.scale)
        model = glm.rotate(model, this.rotate.angle, this.rotate.vector)
        this.scene.shader.setMatrix4("model", model.elements)

        this.scene.shader.setInteger("enableTexture", 0)

        this.properties.forEach((property) => {
            property.beforeAction()
        })

        this.properties.forEach((property) => {
            property.action()
        })

        this.properties.forEach((property) => {
            property.afterAction()
        })
    }

    addProperty(property) {
        this.properties.set(property.constructor.name, property)
        property.entity = this
        if (this.scene) property.init()
    }

    removeProperty(name) {
        this.properties.get(name).destroy()
        this.properties.delete(name)
    }
}
