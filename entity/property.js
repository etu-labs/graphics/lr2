export class Property {
    entity = null

    constructor() {}

    // Выполняется единовременно при добавлении свойства
    init() {}

    // Повторяется каждый кадр
    beforeAction() {}
    action() {}
    afterAction() {}

    // Выполняется единовременно при удалении свойства
    destroy() {}
}
