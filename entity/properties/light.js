import {Property} from "../property.js";

export class Light extends Property {
    // distance 13
    constant = 1.0
    linear = 0.35
    quadratic = 0.44

    ambient = glm.vec3(0.6)
    diffuse = glm.vec3(0.6)
    specular = glm.vec3(0.6)

    material = null

    init() {
        super.init()

        this.entity.scene.lights.push(this.entity)

        this.material = this.entity.properties.get("Material")
    }

    destroy() {
        super.destroy()

        let index = this.entity.scene.lights.findIndex(entity => {
            return entity === this.entity
        })
        this.entity.scene.lights.splice(index, 1)
    }
}
