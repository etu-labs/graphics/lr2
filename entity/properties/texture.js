import {Property} from "../property.js";
import {gl} from "../../drawer.js";

export class Texture extends Property {
    #texture = null
    #image = null
    #isReady = false
    #name = null
    #data = null

    constructor(options) {
        super()
        this.#name = options.name
        this.#data = options.data
    }

    init() {
        super.init()
        this.#loadTexture().then(() => {
            this.#isReady = true
        })
    }

    beforeAction() {
        super.beforeAction()

        this.entity.scene.shader.setInteger("enableTexture", 1)

        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, this.#texture)
    }

    afterAction() {
        super.afterAction()

        gl.activeTexture(null)
    }

    // todo load image from file
    #loadFromFile(url) {
        return new Promise(resolve => {
            const image = new Image();
            image.addEventListener('load', () => {
                resolve(image);
            });
            image.src = url;
        });
    }

    async #loadFromData(data) {
        const url = await this.#readURL(data);
        const image = new Image()
        image.src = url;
        return image
    }

    async #readURL(file) {
        return new Promise((res, rej) => {
            const reader = new FileReader();
            reader.onload = e => res(e.target.result);
            reader.onerror = e => rej(e);
            reader.readAsDataURL(file);
        });
    }

    async #loadTexture() {
        this.#texture = gl.createTexture()
        if (this.#name) {
            this.#image = await this.#loadFromFile("data/textures/" + this.#name)
        } else {
            this.#image = await this.#loadFromData(this.#data)
        }

        gl.bindTexture(gl.TEXTURE_2D, this.#texture)
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, this.#image.width, this.#image.height, 0, gl.RGB, gl.UNSIGNED_BYTE, this.#image)
        gl.generateMipmap(gl.TEXTURE_2D)

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR)
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

        gl.bindTexture(gl.TEXTURE_2D, null)
    }
}
