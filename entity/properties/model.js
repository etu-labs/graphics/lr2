import {Property} from "../property.js";
import {gl} from "../../drawer.js";

export class Model extends Property{
    #vertices = [
        -0.5, -0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,
        0.5, -0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,
        0.5,  0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,
        0.5,  0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,
        -0.5,  0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,
        -0.5, -0.5, -0.5,  0.0,  0.0, -1.0, 0.0, 0.0,

        -0.5, -0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,
        0.5, -0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,
        0.5,  0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,
        0.5,  0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,
        -0.5,  0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,
        -0.5, -0.5,  0.5,  0.0,  0.0,  1.0, 0.0, 0.0,

        -0.5,  0.5,  0.5, -1.0,  0.0,  0.0, 0.0, 0.0,
        -0.5,  0.5, -0.5, -1.0,  0.0,  0.0, 0.0, 0.0,
        -0.5, -0.5, -0.5, -1.0,  0.0,  0.0, 0.0, 0.0,
        -0.5, -0.5, -0.5, -1.0,  0.0,  0.0, 0.0, 0.0,
        -0.5, -0.5,  0.5, -1.0,  0.0,  0.0, 0.0, 0.0,
        -0.5,  0.5,  0.5, -1.0,  0.0,  0.0, 0.0, 0.0,

        0.5,  0.5,  0.5,  1.0,  0.0,  0.0, 0.0, 0.0,
        0.5,  0.5, -0.5,  1.0,  0.0,  0.0, 0.0, 0.0,
        0.5, -0.5, -0.5,  1.0,  0.0,  0.0, 0.0, 0.0,
        0.5, -0.5, -0.5,  1.0,  0.0,  0.0, 0.0, 0.0,
        0.5, -0.5,  0.5,  1.0,  0.0,  0.0, 0.0, 0.0,
        0.5,  0.5,  0.5,  1.0,  0.0,  0.0, 0.0, 0.0,

        -0.5, -0.5, -0.5,  0.0, -1.0,  0.0, 0.0, 0.0,
        0.5, -0.5, -0.5,  0.0, -1.0,  0.0, 0.0, 0.0,
        0.5, -0.5,  0.5,  0.0, -1.0,  0.0, 0.0, 0.0,
        0.5, -0.5,  0.5,  0.0, -1.0,  0.0, 0.0, 0.0,
        -0.5, -0.5,  0.5,  0.0, -1.0,  0.0, 0.0, 0.0,
        -0.5, -0.5, -0.5,  0.0, -1.0,  0.0, 0.0, 0.0,

        -0.5,  0.5, -0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
        0.5,  0.5, -0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
        0.5,  0.5,  0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
        0.5,  0.5,  0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
        -0.5,  0.5,  0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
        -0.5,  0.5, -0.5,  0.0,  1.0,  0.0, 0.0, 0.0,
    ]
    #numberVertices = 0

    #VAO = null
    #VBO = null


    constructor(vertices) {
        super()
        if (vertices) {
            this.#vertices = vertices
        }

        this.#numberVertices = this.#vertices.length / 8
    }

    init() {
        super.init()

        this.#VAO = gl.createVertexArray()
        this.#VBO = gl.createBuffer()
        let verticesFloat32Array = new Float32Array(this.#vertices)
        let FSIZE = verticesFloat32Array.BYTES_PER_ELEMENT

        gl.bindBuffer(gl.ARRAY_BUFFER, this.#VBO)
        gl.bufferData(gl.ARRAY_BUFFER, verticesFloat32Array, gl.STATIC_DRAW)

        gl.bindVertexArray(this.#VAO)

        let attrPosLoc = this.entity.scene.shader.getAttribLocation("aPos")
        gl.vertexAttribPointer(attrPosLoc, 3, gl.FLOAT, false, FSIZE * 8, 0)
        gl.enableVertexAttribArray(attrPosLoc)

        let attrNormalLoc = this.entity.scene.shader.getAttribLocation("aNormal")
        gl.vertexAttribPointer(attrNormalLoc, 3, gl.FLOAT, false, FSIZE * 8, FSIZE * 3)
        gl.enableVertexAttribArray(attrNormalLoc)

        let attrTexLoc = this.entity.scene.shader.getAttribLocation("aTexCoords")
        gl.vertexAttribPointer(attrTexLoc, 2, gl.FLOAT, false, FSIZE * 8, FSIZE * 6)
        gl.enableVertexAttribArray(attrTexLoc)

        gl.bindVertexArray(null);
    }

    action() {
        super.action()

        gl.bindVertexArray(this.#VAO)
        gl.drawArrays(gl.TRIANGLES, 0, this.#numberVertices)
        gl.bindVertexArray(null)
    }
}
