import {Property} from "../property.js";
import {Light} from "./light.js";

export class Material extends Property {
    ambient = glm.vec3(1.0, 1.0, 1.0)
    diffuse = glm.vec3(1.0, 1.0, 1.0)
    specular = glm.vec3(0.4, 0.4, 0.4)
    shininess = 32.0


    constructor(ambient, diffuse, specular, shininess) {
        super()
        if (ambient) {
            this.ambient = ambient
        }
        if (diffuse) {
            this.diffuse = diffuse
        }
        if (specular) {
            this.specular = specular
        }
        if (shininess) {
            this.shininess = shininess
        }
    }

    init() {
        super.init()

        let light = this.entity.properties.get(Light.name)
        if (light) {
            light.material = this
        }
    }

    beforeAction() {
        super.beforeAction()

        this.entity.scene.shader.setVector3f("material.ambient", this.ambient);
        this.entity.scene.shader.setVector3f("material.diffuse", this.diffuse);
        this.entity.scene.shader.setVector3f("material.specular", this.specular);
        this.entity.scene.shader.setFloat("material.shininess", this.shininess);
    }
}
